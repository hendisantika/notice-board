package com.hendisantika.noticeboard.controller;

import com.hendisantika.noticeboard.dto.BoardDTO;
import com.hendisantika.noticeboard.service.BoardService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : notice-board
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/03/21
 * Time: 07.23
 */
@RestController
@RequestMapping("/boards")
public class BoardController extends CrudController<BoardDTO> {

    public BoardController(BoardService boardService) {
        super(boardService);
    }
}