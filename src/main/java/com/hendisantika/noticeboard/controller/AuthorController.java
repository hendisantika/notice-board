package com.hendisantika.noticeboard.controller;

import com.hendisantika.noticeboard.dto.AuthorDTO;
import com.hendisantika.noticeboard.service.AuthorService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : notice-board
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/03/21
 * Time: 07.22
 */

@RestController
@RequestMapping("/authors")
public class AuthorController extends CrudController<AuthorDTO> {

    public AuthorController(AuthorService authorService) {
        super(authorService);
    }
}
