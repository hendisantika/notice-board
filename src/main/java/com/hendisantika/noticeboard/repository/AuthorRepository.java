package com.hendisantika.noticeboard.repository;

import com.hendisantika.noticeboard.entity.Author;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : notice-board
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/03/21
 * Time: 09.03
 */
@Repository
public interface AuthorRepository extends CrudRepository<Author, Long> {
}
