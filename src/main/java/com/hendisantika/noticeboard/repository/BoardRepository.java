package com.hendisantika.noticeboard.repository;

import com.hendisantika.noticeboard.entity.Board;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : notice-board
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/03/21
 * Time: 09.04
 */
@Repository
public interface BoardRepository extends CrudRepository<Board, Long> {
}