package com.hendisantika.noticeboard.mapper;

import com.hendisantika.noticeboard.dto.NoticeDTO;
import com.hendisantika.noticeboard.entity.Notice;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Created by IntelliJ IDEA.
 * Project : notice-board
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/03/21
 * Time: 07.30
 */
@Mapper
public interface NoticeMapper {

    NoticeMapper INSTANCE = Mappers.getMapper(NoticeMapper.class);

    NoticeDTO noticeToDto(Notice notice);

    Notice dtoToNotice(NoticeDTO noticeDTO);
}