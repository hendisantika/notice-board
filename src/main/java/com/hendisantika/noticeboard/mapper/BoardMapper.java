package com.hendisantika.noticeboard.mapper;

import com.hendisantika.noticeboard.dto.BoardDTO;
import com.hendisantika.noticeboard.entity.Board;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Created by IntelliJ IDEA.
 * Project : notice-board
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/03/21
 * Time: 07.30
 */
@Mapper
public interface BoardMapper {

    BoardMapper INSTANCE = Mappers.getMapper(BoardMapper.class);

    BoardDTO boardToDto(Board board);

    Board dtoToBoard(BoardDTO boardDTO);
}
