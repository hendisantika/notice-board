package com.hendisantika.noticeboard.mapper;

import com.hendisantika.noticeboard.dto.AuthorDTO;
import com.hendisantika.noticeboard.entity.Author;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Created by IntelliJ IDEA.
 * Project : notice-board
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/03/21
 * Time: 09.06
 */
@Mapper
public interface AuthorMapper {

    AuthorMapper INSTANCE = Mappers.getMapper(AuthorMapper.class);

    AuthorDTO authorToDto(Author author);

    Author dtoToAuthor(AuthorDTO authorDTO);
}