package com.hendisantika.noticeboard.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Created by IntelliJ IDEA.
 * Project : notice-board
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/03/21
 * Time: 09.09
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Notice")
public class NoticeDTO extends BaseDTO {

    @ApiModelProperty(value = "Notice title")
    private String title;

    @ApiModelProperty(value = "Notice detailed description")
    private String description;

    @ApiModelProperty(value = "A Person who created this Notice")
    private AuthorDTO author;
}
