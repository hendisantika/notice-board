package com.hendisantika.noticeboard.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Created by IntelliJ IDEA.
 * Project : notice-board
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/03/21
 * Time: 09.07
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class AuthorDTO extends BaseDTO {

    private String firstName;

    private String lastName;
}