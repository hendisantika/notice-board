package com.hendisantika.noticeboard.service;

import com.hendisantika.noticeboard.dto.BoardDTO;
import com.hendisantika.noticeboard.entity.Board;
import com.hendisantika.noticeboard.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.hendisantika.noticeboard.mapper.BoardMapper.INSTANCE;

/**
 * Created by IntelliJ IDEA.
 * Project : notice-board
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/03/21
 * Time: 07.44
 */
@Service
@RequiredArgsConstructor
public class BoardService implements CrudService<BoardDTO> {

    private final BoardRepository boardRepository;

    @Override
    public List<BoardDTO> findAll() {
        List<BoardDTO> boardDTOList = new ArrayList<>();
        boardRepository.findAll().forEach(board -> boardDTOList.add(INSTANCE.boardToDto(board)));
        return boardDTOList;
    }

    @Override
    public Optional<BoardDTO> findById(Long id) {
        Optional<Board> boardOptional = boardRepository.findById(id);
        return boardOptional.map(INSTANCE::boardToDto);
    }

    @Override
    public BoardDTO save(BoardDTO boardDTO) {
        Board board = INSTANCE.dtoToBoard(boardDTO);
        return INSTANCE.boardToDto(boardRepository.save(board));
    }

    @Override
    public void delete(Long id) {
        boardRepository.deleteById(id);
    }

    @Override
    public BoardDTO update(Long id, BoardDTO boardDTO) {
        Board savedBoard = boardRepository.findById(id).orElseThrow();
        Board boardToUpdate = INSTANCE.dtoToBoard(boardDTO);

        savedBoard.setTitle(boardToUpdate.getTitle());
        savedBoard.setNoticeList(boardToUpdate.getNoticeList());

        return INSTANCE.boardToDto(boardRepository.save(savedBoard));
    }
}
